# Deploy postgress on aks

## create database namespace
~~~
kubectl create namespace database
~~~

## Install the PostgreSQL chart from Bitnami using Helm:

~~~
helm install my-release oci://registry-1.docker.io/bitnamicharts/postgresql --namespace database
~~~

## added the "azure-marketplace" Helm repository
~~~
helm repo add azure-marketplace https://marketplace.azurecr.io/helm/v1/repo
~~~

## update
~~~
helm repo update
~~~


## see the password for the postgress
~~~
kubectl get secret --namespace database my-release-postgresql -o jsonpath="{.data.postgres-password}" | base64 --decode
~~~


## connect to postgress
~~~
kubectl run my-release-postgresql-client --rm --tty -i --restart='Never' --namespace database --image marketplace.azurecr.io/bitnami/postgresql:15.1.0-debian-11-r0 --env="PGPASSWORD=<output from previous command>" --command -- psql --host my-release-postgresql -U postgres -d postgres -p 5432
~~~

## port forward(if want)
~~~
kubectl port-forward --namespace database svc/my-release-postgresql 5432:5432
~~~


## create a database
~~~
CREATE DATABASE phonebookdb;
~~~


## connect to phonebookdb database
~~~
kubectl exec -it my-release-postgresql-0 --namespace database -- psql --host my-release-postgresql -U postgres -d phonebookdb -p 5432
~~~


# Connect the Database on GCP
~~~
psql -h 34.79.23.69 -U postgres -d postgres
XPgfSh2OMe
~~~

## build the table for the project
~~~
CREATE TABLE photogame (
    id SERIAL PRIMARY KEY,
    image_link TEXT
   
);                 
~~~
